import numpy as np
import cv2

#camera = cv2.VideoCapture('SampleVideo_640x360_2mb.mp4') #sample video from http://www.sample-videos.com/
camera = cv2.VideoCapture(0) # open the webcam for video streaming
	
count = 0
while True:
	frame, image = camera.read()
	cv2.imshow("webcam", image) #show the webcam window
	cv2.imwrite('frame%d.jpg'% count, image) #save frame of the video streaming as jpeg
	if(cv2.waitKey(10) == 27): #exit if Escape key is hit
		break
	 
	count += 1

